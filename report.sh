#!/bin/bash
# Menu to generate reports on who's been misbehaving on your server. 
# Fail2Ban must be installed and configured.
# Sarah Evans @devhops. I like bash and don't give a fuck.

while :
do
  clear
  echo "Fail2Ban Report Generator for the lazy sysadmin"
  echo "1. Find most common attempts from IP's "
  echo "2. Check number of attempts "
  echo "3. Group by IP "
  echo "4. Group by IP from all (old) logfiles "
  echo "5. Exit "
  echo -n "Please select an option [1-5]"
  read opt
  case $opt in
	1) echo "Find most common attempts from IP's ";
	zgrep -h "Ban " /var/log/fail2ban/fail2ban.log* | awk '{print $NF}' | sort | uniq -c;
        echo "Press [enter] to continue";
           read enterKey;;
	2) echo "Check number of attempts ";
        zcat /var/log/fail2ban/secure* | grep 'Failed password' | grep sshd | awk '{print $1,$2}' | sort -k 1,1M -k 2n | uniq -c;
        echo "Press [enter] to continue";
           read enterKey;;
	3) echo "Group by IP";
	awk '($(NF-1) = /Ban/){print $NF}' /var/log/fail2ban/fail2ban.log | sort | uniq -c | sort -n;
                echo "Press [enter] to continue";
           read enterKey;;
	4) echo "Group by IP from all logfiles";
	zgrep -h "Ban " /var/log/fail2ban/fail2ban.log* | awk '{print $NF}' | sort | uniq -c;
                echo "Press [enter] to continue";
           read enterKey;;
	5) echo "Exiting..."
	exit 1;;
	*) echo "I'm sorry Dave, I can't do that."
	   echo "Press [enter] to continue";
           read enterKey;;
esac
done
