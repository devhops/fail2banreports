Fail2Ban Reports
================

Quick and dirty bash menu to avoid me having to run commands frequently to 
check how fail2ban is doing.

This was written for a CentOS system, so the log file locations reflect that. 
If you're using Debian, change it to auth.log instead of secure.

If you're using Ubuntu, don't speak to me.